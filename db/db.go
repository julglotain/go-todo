package db

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/boltdb/bolt"
	"github.com/google/uuid"
	"github.com/mitchellh/go-homedir"
)

const (
	TodosBucket string = "Todos"
)

type Todo struct {
	Id        string    `json:"id"`
	Desc      string    `json:"desc"`
	CreatedAt time.Time `json:"createdAt"`
	Done      bool      `json:"done"`
}

func (t *Todo) Toggle() {
	t.Done = !t.Done
}

func Create(desc string) {
	db := GetTodoDb()
	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("Todos"))
		// create a random uuid
		nuuid, errMarshalling := uuid.New().MarshalText()
		if errMarshalling != nil {
			log.Fatal("err marshalling uuid")
		}
		todo := Todo{Id: string(nuuid), Desc: desc, CreatedAt: time.Now(), Done: false}
		entryBytes, err := json.Marshal(todo)
		err = b.Put(nuuid, entryBytes)
		if err != nil {
			return fmt.Errorf("could not insert entry: %v", err)
		}
		return nil
	})
	if err != nil {
		log.Fatal("err create new todo")
	}
	fmt.Println("new todo added: ", desc)
	defer db.Close()
}

func init() {
	db := GetTodoDb()
	defer db.Close()
	// creation du bucket si non existant
	db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(TodosBucket))
		if err != nil {
			return fmt.Errorf("could not create Todos bucket: %v", err)
		}
		return nil
	})
}

func GetTodoDb() *bolt.DB {
	home, err := homedir.Dir()
	todoHomePath := home + "/.todo"
	if _, err := os.Stat(todoHomePath); os.IsNotExist(err) {
		err := os.Mkdir(todoHomePath, os.ModePerm)
		if err != nil {
			log.Fatal(err)
		}
	}
	dbPath := todoHomePath + "/todo.db"
	db, err := bolt.Open(dbPath, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	return db
}

func GetAllTodos() []Todo {
	db := GetTodoDb()
	defer db.Close()
	todos := []Todo{}
	db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(TodosBucket))
		b.ForEach(func(k, v []byte) error {
			// fmt.Printf("key=%s, value=%s\n", k, v)
			var todo Todo
			errUnmarshalling := json.Unmarshal(v, &todo)
			if errUnmarshalling != nil {
				log.Fatal("Error unmarshalling todo")
			}
			todos = append(todos, todo)
			// fmt.Println(todos)
			if todos == nil {
				log.Fatal("Error appending todo")
			}
			return nil
		})
		return nil
	})
	return todos
}

func GetTodo(id string) (Todo, error) {
	db := GetTodoDb()
	defer db.Close()
	var todo Todo
	db.View(func(tx *bolt.Tx) error {
		entry := tx.Bucket([]byte(TodosBucket)).Get([]byte(id))
		err := json.Unmarshal(entry, &todo)
		if err != nil {
			return fmt.Errorf("getting todo: %s", err)
		}
		return nil
	})
	return todo, nil
}

func ToggleTodo(id string) (Todo, error) {
	db := GetTodoDb()
	defer db.Close()
	var todo Todo
	errUpdate := db.Update(func(tx *bolt.Tx) error {
		entry := tx.Bucket([]byte(TodosBucket)).Get([]byte(id))
		err := json.Unmarshal(entry, &todo)
		if err != nil {
			return fmt.Errorf("getting todo: %s", err)
		}
		todo.Toggle()
		entryBytes, err := json.Marshal(todo)
		errPut := tx.Bucket([]byte(TodosBucket)).Put([]byte(id), entryBytes)
		if errPut != nil {
			fmt.Errorf("put %v", errPut)
		}
		return nil
	})
	return todo, errUpdate
}
