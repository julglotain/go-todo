package web

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/julglotain/go-todo/db"
)

// ListTodosHandler is a http handler returning list of all todos
func ListTodosHandler(w http.ResponseWriter, r *http.Request) {
	todos := db.GetAllTodos()
	payload, err := json.Marshal(todos)
	if err != nil {
		log.Fatal(err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(payload)
}

func GetTodoHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	todo, err := db.GetTodo(vars["id"])
	if err != nil {
		w.WriteHeader(404)
		return
	}
	payload, err := json.Marshal(&todo)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(payload)
}

func ToggleTodoState(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	todoId := vars["id"]

	db.ToggleTodo(todoId)
}
